#pragma once
#include "Component.h"
class Entity :
	public Component
{
public:
	Entity();
	~Entity();
	void Jump();
	bool GetIsJumping();
	void Update(int interpolate);
protected:
	bool _isJumping;
};

