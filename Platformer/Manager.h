#pragma once
#include "library.h"
class Manager
{
protected:
	static Manager* _instance;
public:
	Manager();
	~Manager();
	static Manager* GetInstance();
};

