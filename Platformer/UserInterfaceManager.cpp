#include "UserInterfaceManager.h"


UserInterfaceManager::UserInterfaceManager(RenderEngine* renderer)
{
	_renderer = renderer;

	// load font.ttf at size 16 into font
	_font = TTF_OpenFont("assets/fonts/Jura-DemiBold.ttf", 16);
	if (!_font) {
		printf("TTF_OpenFont: %s\n", TTF_GetError());
	}
	LoadUserInterface();
}

UserInterfaceManager::~UserInterfaceManager()
{
}

void UserInterfaceManager::Update(int interpolate)
{
	auto compEnd = _uiComponents.end();

	for (auto compIt = _uiComponents.begin(); compIt != compEnd; compIt++)
	{
		UserInterfaceComponent* uiComp = *compIt;

		uiComp->Update(interpolate);
	}
}

void UserInterfaceManager::Draw(int interpolate)
{
	auto compEnd = _uiComponents.end();

	for (auto compIt = _uiComponents.begin(); compIt != compEnd; compIt++)
	{
		UserInterfaceComponent* uiComp = *compIt;

		_renderer->DrawText(uiComp, _font);
	}
}

void UserInterfaceManager::LoadUserInterface()
{
	auto healthDisplay = new UserInterfaceComponent();
	healthDisplay->SetLabelText("Health: ");
	healthDisplay->SetValueText("100");
	healthDisplay->SetPosition(b2Vec2(25, 25));
	_uiComponents.push_back(healthDisplay);
}
