#include "Entity.h"


Entity::Entity()
{
}


Entity::~Entity()
{
}

void Entity::Jump()
{
	if (_isJumping)
	{
		return;
	}

	_isJumping = true;

	_body->ApplyLinearImpulse(b2Vec2(0, -25), _body->GetWorldCenter(), true);
	_velocityChanged = true;
}


bool Entity::GetIsJumping()
{
	return _isJumping;
}


void Entity::Update(int interpolate)
{
	if (_isJumping)
	{
		//Check if the foot sensor is colliding
			//Set isJumping to false
	}
	Component::Update(interpolate);
}
