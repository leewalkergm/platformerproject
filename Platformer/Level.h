#pragma once
#include "library.h"
#include "Component.h"
#include "TmxParser\TmxTileset.h"

class Level
{
protected:
	std::vector<Component*> _components;
	b2World* _world;
	b2Vec2 _size;
	const Tmx::Tileset* _tileSet;
	std::vector<std::vector<std::vector<Component*>>>  _tiles;

public:
	Level();
	~Level();

	const std::vector<std::vector<std::vector<Component*>>> * GetTiles();
	const std::vector<Component*>* GetComponents();
	b2World* GetWorld();

	void SetSize(b2Vec2 size);
	void SetTileset(const Tmx::Tileset* tileset);
	const Tmx::Tileset* GetTileset();
	void SetTiles(std::vector<std::vector<std::vector<Component*>>>);
	void SetComponents(std::vector<Component*> components);
};