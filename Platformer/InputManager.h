#pragma once
#include "LevelManager.h"
#include "Entity.h"

class InputManager
{
public:
	InputManager(LevelManager*);
	~InputManager();
	bool PollEvent(bool);
	void HandleKeyPress();
protected:
	LevelManager* _levelManager;
	bool _keysHeld[323];
};
