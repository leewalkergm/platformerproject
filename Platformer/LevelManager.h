#pragma once
#include "library.h"
#include "Level.h"
#include "ResourceManager.h"
#include "RenderEngine.h"
#include "UserInterfaceManager.h"
#include "Entity.h"
#include "MathHelper.h"
#include "TmxParser\TmxMap.h"
#include "TmxParser\TmxImage.h"
#include "TmxParser\TmxLayer.h"
#include "TmxParser\TmxTile.h"
#include "TmxParser\TmxObject.h"
#include "TmxParser\TmxObjectGroup.h"
#include "TmxParser\TmxTileset.h"

class LevelManager
{
public:
	LevelManager(SDL_Renderer*);
	~LevelManager();
	void Update(int interpolate);
	void Draw(int interpolate);
	void LoadLevel(std::string levelname);
	void SetPhysics(Component* component, b2Vec2, b2Vec2, b2BodyType);
	void LoadTiles(Tmx::Map*);
	void LoadEntities(Tmx::Map*);
	void UpdateCamera();
	Entity* GetPlayer();
protected:
	Level* _level;
	ResourceManager* _resourceManager;
	RenderEngine* _renderEngine;
	SDL_Renderer* _renderer;
	Entity* _player;
	UserInterfaceManager* _uiManager;
};

