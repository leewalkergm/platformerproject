#include "RenderEngine.h"

RenderEngine::RenderEngine(SDL_Renderer* renderer)
{
	_renderer = renderer;

	_camera = new SDL_Rect();
	_camera->x = 0;
	_camera->y = 0;
	_camera->w = VIEWPORT_WIDTH;
	_camera->h = VIEWPORT_HEIGHT;
}


RenderEngine::~RenderEngine()
{
}


void RenderEngine::Draw(Component* component)
{
	b2Vec2 pos = component->GetPosition();
	b2Vec2 size = component->GetSize();
	SDL_Rect offs;
	offs.x = pos.x - _camera->x;
	offs.y = pos.y - _camera->y;
	offs.w = size.x;
	offs.h = size.y;

	SDL_RenderCopy(_renderer, component->GetTexture(), component->GetClipping(), &offs);
}


void RenderEngine::DrawStatic(Component* component)
{
	b2Vec2 pos = component->GetPosition();
	b2Vec2 size = component->GetSize();

	SDL_Rect offs;
	offs.x = pos.x;
	offs.y = pos.y;
	offs.w = size.x;
	offs.h = size.y;

	SDL_RenderCopy(_renderer, component->GetTexture(), component->GetClipping(), &offs);
}


SDL_Rect* RenderEngine::GetCamera()
{
	return _camera;
}


void RenderEngine::MoveCamera(int x, int y)
{
	_camera->x += x;
	_camera->y += y;
	_camera->w = VIEWPORT_WIDTH;
	_camera->h = VIEWPORT_HEIGHT;
}


void RenderEngine::SetCamera(SDL_Rect* rect)
{
	_camera = rect;
}


void RenderEngine::DrawText(UserInterfaceComponent* component, TTF_Font* font)
{
	SDL_Color color = { 255, 255, 255, 255 };


	b2Vec2 pos, size;
	pos = component->GetPosition();
	size = component->GetSize();

	int w, h;

	std::string labelText = component->GetLabelText();
	std::string valueText = component->GetValueText();

	const char* totalText = labelText.append(valueText).c_str();

	if (TTF_SizeText(font, totalText, &w, &h) != 0)
	{
		std::cout << "Error!" << std::endl;
	}

	SDL_Surface* message;
	if (component->GetTexture() == nullptr)
	{
		message = TTF_RenderText_Solid(font, totalText, color);

		SDL_Texture* hudTex = SDL_CreateTextureFromSurface(_renderer, message);
		component->SetTexture(hudTex);
		SDL_FreeSurface(message);
	}

	SDL_Rect offs;
	offs.x = pos.x;
	offs.y = pos.y;
	offs.w = w;
	offs.h = h;

	SDL_RenderCopy(_renderer, component->GetTexture(), nullptr, &offs);
}
