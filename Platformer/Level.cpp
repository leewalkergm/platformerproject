#include "Level.h"


Level::Level()
{
	b2Vec2 gravity = GRAVITY;
	_world = new b2World(gravity);
}


Level::~Level()
{
	for (Component* comp : _components)
	{
		delete comp;
	}
}


const std::vector<Component*>* Level::GetComponents()
{
	return &_components;
}


b2World* Level::GetWorld()
{
	return _world;
}


void Level::SetSize(b2Vec2 size)
{
	_size = size;
}


void Level::SetTileset(const Tmx::Tileset* tileset)
{
	_tileSet = tileset;
}


const Tmx::Tileset* Level::GetTileset()
{
	return _tileSet;
}


void Level::SetTiles(std::vector<std::vector<std::vector<Component*>>>  tiles)
{
	_tiles = tiles;
}


const std::vector<std::vector<std::vector<Component*>>> * Level::GetTiles()
{
	return &_tiles;
}


void Level::SetComponents(std::vector<Component*> components)
{
	_components = components;
}
