//Graphics settings
#define VIEWPORT_WIDTH 640
#define VIEWPORT_HEIGHT 480
#define FULLSCREEN false

#define UPDATES_PER_SECOND 60

#define MOVEMENT_SPEED 1
#define MAX_MOVEMENT_SPEED 10

//Physics
#define GRAVITY b2Vec2(0.0f, 15.0f)
#define SCALING_FACTOR 50

#define BLOCK_SIZE 32