#include "InputManager.h"


InputManager::InputManager(LevelManager* levelManager)
{
	_levelManager = levelManager;
	_keysHeld[323] = { false };
}


InputManager::~InputManager()
{
	_levelManager = nullptr;
}

bool InputManager::PollEvent(bool GAME_IS_RUNNING)
{
	SDL_Event ev;
	if (SDL_PollEvent(&ev)) 
	{
		if (ev.type == SDL_QUIT) 
		{
			GAME_IS_RUNNING = false;
			std::cout << "Quit event. " << std::endl;
		}

		auto key = ev.key.keysym.sym;
		if (ev.type == SDL_KEYDOWN)
		{
			if (key == SDLK_ESCAPE)
			{
				GAME_IS_RUNNING = false;
			}

			if (key < 324)
			{
				_keysHeld[key] = true;
			}
		}
		if (ev.type == SDL_KEYUP)
		{
			if (key < 324)
			{
				_keysHeld[key] = false;
			}
		}
	}

	HandleKeyPress();

	return GAME_IS_RUNNING;
}


void InputManager::HandleKeyPress()
{
	Entity* player = _levelManager->GetPlayer();

	if (_keysHeld[SDLK_a])
	{
		player->ApplyVelocity(b2Vec2(-MOVEMENT_SPEED, 0));
	}
	if (_keysHeld[SDLK_d])
	{
		player->ApplyVelocity(b2Vec2(MOVEMENT_SPEED, 0));
	}
	if (_keysHeld[SDLK_w] || _keysHeld[SDLK_SPACE])
	{
		player->Jump();
	}

	player = nullptr;
}
