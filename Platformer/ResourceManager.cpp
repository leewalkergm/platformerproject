#include "ResourceManager.h"


ResourceManager::ResourceManager(SDL_Renderer* renderer)
{
	_renderer = renderer;
}


ResourceManager::~ResourceManager()
{
	_renderer = nullptr;
	for (auto texturePair : _loadedTextures)
	{
		SDL_Texture* texture = texturePair.second;
		texture = nullptr;
	}
	_loadedTextures.clear();
}


SDL_Texture* ResourceManager::LoadTexture(std::string fileName)
{
	SDL_Texture* texture;
	if (_loadedTextures.find(fileName) != _loadedTextures.end())
	{
		texture = _loadedTextures.at(fileName);
	}
	else
	{
		//std::cout << "Loading -- " << fileName << std::endl;
		texture = IMG_LoadTexture(_renderer, fileName.c_str());
		if (texture == nullptr)
		{
			//std::cout << fileName << " - couldn't be found" << std::endl;
		}
		else
		{
			_loadedTextures.emplace(fileName, texture);
		}
	}
	return texture;
}
