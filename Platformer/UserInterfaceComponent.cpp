#include "UserInterfaceComponent.h"


UserInterfaceComponent::UserInterfaceComponent()
{
}


UserInterfaceComponent::~UserInterfaceComponent()
{
	_font = nullptr;
}


void UserInterfaceComponent::SetLabelText(std::string labelText)
{
	_labelText = labelText;
	_totalText = _labelText + _valueText;
}


void UserInterfaceComponent::SetValueText(std::string valueText)
{
	_valueText = valueText;
	_totalText = _labelText + _valueText;
}


std::string UserInterfaceComponent::GetLabelText()
{
	return _labelText;
}


std::string UserInterfaceComponent::GetValueText()
{
	return _valueText;
}


SDL_Texture* UserInterfaceComponent::GetTexture()
{
	return _texture;
}


void UserInterfaceComponent::Update(int interpolate)
{
}

std::string UserInterfaceComponent::GetText()
{
		return _totalText;
}