#include "Component.h"


Component::Component()
{
	_isNearScreen = true;
	_health = 0;
}


Component::~Component()
{
	_body = nullptr;
	_texture = nullptr;
}


b2Vec2 Component::GetPosition()
{
	return _pos;
}


b2Vec2 Component::GetSize()
{
	return _size;
}


void Component::Update(int interpolate)
{
	if (_body != nullptr)
	{
		auto pos = _body->GetPosition();
		_pos.x = pos.x*SCALING_FACTOR;
		_pos.y = pos.y*SCALING_FACTOR;
		if (_velocityChanged)
		{
			_body->SetLinearVelocity(_velocity);
			_velocityChanged = false;
		}
		_velocity = _body->GetLinearVelocity();
	}
}

void Component::SetPosition(b2Vec2 position)
{
	_pos = position;
}


void Component::SetSize(b2Vec2 size)
{
	_size = size;
}

void Component::SetTexture(SDL_Texture* texture)
{
	_texture = texture;
}


SDL_Texture* Component::GetTexture()
{
	return _texture;
}


SDL_Rect* Component::GetClipping()
{
	return &_clipping;
}


void Component::SetClipping(SDL_Rect* clipping)
{
	_clipping = *clipping;
}

void Component::SetBody(b2Body* body)
{
	_body = body;
}


SDL_Rect* Component::GetRect()
{
	SDL_Rect* rect = new SDL_Rect();
	rect->x = _pos.x;
	rect->y = _pos.y;

	rect->x = _size.x;
	rect->y = _size.y;
	return rect;
}

void Component::ApplyVelocity(b2Vec2 vel)
{
	_velocity.x += vel.x == 0 ? 0 : vel.x;
	_velocity.y += vel.y == 0 ? 0 : vel.y;

	if (abs(_velocity.x) > MAX_MOVEMENT_SPEED)
	{
		_velocity.x = _velocity.x > 0 ? MAX_MOVEMENT_SPEED : -MAX_MOVEMENT_SPEED;
	}

	_velocityChanged = true;
}

bool Component::GetIsNearScreen()
{
	return _isNearScreen;
}

int Component::GetHealth()
{
	return _health;
}

//void Component::SetHealth(int health)
//{
//	_health = health;
//}

void Component::InflictDamage(int damage)
{
	_health -= damage;
}
