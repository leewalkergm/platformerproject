#pragma once
#include "library.h"
#include "RenderEngine.h"
#include "UserInterfaceComponent.h"
class UserInterfaceManager
{
protected:
	std::vector<UserInterfaceComponent*> _uiComponents;
	RenderEngine* _renderer;
public:
	UserInterfaceManager(RenderEngine*);
	~UserInterfaceManager();
	void Update(int interpolate);
	void Draw(int interpolate);
	void LoadUserInterface();
protected:
	TTF_Font* _font;
};

