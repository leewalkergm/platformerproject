#pragma once
#include "library.h"

class ResourceManager
{
protected:
	std::map<std::string, SDL_Texture*> _loadedTextures;
	SDL_Renderer* _renderer;
public:
	ResourceManager(SDL_Renderer* renderer);
	~ResourceManager();
	SDL_Texture* LoadTexture(std::string fileName);
};
