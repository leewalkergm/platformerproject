#pragma once
#include "library.h"
#include "Component.h"
class UserInterfaceComponent :
	public Component
{
protected:
	std::string _labelText;
	std::string _valueText;
	std::string _totalText;
	TTF_Font* _font;
public:
	UserInterfaceComponent();
	~UserInterfaceComponent();
	void SetLabelText(std::string labelText);
	void SetValueText(std::string valueText);
	std::string GetLabelText();
	std::string GetValueText();
	SDL_Texture* GetTexture();
	std::string GetText();
	void Update(int interpolate);
};

