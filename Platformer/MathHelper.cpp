#include "MathHelper.h"


MathHelper::MathHelper()
{
}


MathHelper::~MathHelper()
{
}


bool MathHelper::SDLRectIntersect(SDL_Rect* r1, SDL_Rect* r2)
{
	bool retVal = (r2->x > r1->x+r1->w ||
		r2->x + r2->w < r1->x ||
		r2->y > r1->y + r1->h||
		r2->y + r2->h < r1->y);

	r1 = nullptr;
	r2 = nullptr;
	return retVal;
}
