#pragma once
#include "library.h"
#include "UserInterfaceComponent.h"
#include "Component.h"
class RenderEngine
{
public:
	RenderEngine(SDL_Renderer* renderer);
	~RenderEngine();
	void Draw(Component* component);
	void DrawStatic(Component* component);
	SDL_Rect* GetCamera();
protected:
	SDL_Renderer* _renderer;
	SDL_Rect* _camera;
public:
	void MoveCamera(int x, int y);
	void SetCamera(SDL_Rect* rect);
	void DrawText(UserInterfaceComponent* component, TTF_Font* font);
};

