#include "LevelManager.h"


LevelManager::LevelManager(SDL_Renderer* renderer)
{
	_renderer = renderer;
	_resourceManager = new ResourceManager(_renderer);
	_renderEngine = new RenderEngine(_renderer);
	_uiManager = new UserInterfaceManager(_renderEngine);

	LoadLevel("assets/levels/tester.tmx");
}

LevelManager::~LevelManager()
{
	_level = nullptr;
	_resourceManager = nullptr;
	_renderEngine = nullptr;
	_renderer = nullptr;
	_player = nullptr;
	_uiManager = nullptr;
}

void LevelManager::Update(int interpolate)
{
	float timeStep = 1.0 / 60.0;
	int velocityUpdates = 6;
	int positionUpdates = 6;

	if (_level == nullptr)
	{
		return;
	}

	_level->GetWorld()->Step(timeStep, velocityUpdates, positionUpdates);


	auto components = _level->GetComponents();

	auto compEnd = components->end();

	for (auto componentBegin = components->begin(); componentBegin != compEnd; componentBegin++)
	{
		auto comp = *componentBegin;
		if (comp != nullptr)
		{
			comp->Update(interpolate);
		}
	}

	if (_player != nullptr)
	{
		UpdateCamera();
	}


	_uiManager->Update(interpolate);
}

void LevelManager::Draw(int interpolate)
{
	if (_level == nullptr)
	{
		return;
	}

	SDL_Rect camera = *_renderEngine->GetCamera();

	const std::vector<std::vector< std::vector<Component*>>>* tiles = _level->GetTiles();

	auto layerEnd = tiles->end();

	int cameraLeft = (camera.x / BLOCK_SIZE) - 1;
	int cameraTop = (camera.y / BLOCK_SIZE) - 1;

	int cameraRight = ((camera.x + camera.w) / BLOCK_SIZE) + 1;
	int cameraBottom = ((camera.y + camera.h) / BLOCK_SIZE) + 1;
	

	//layer
	for (auto tilesBegin = tiles->begin(); tilesBegin != layerEnd; tilesBegin++)
	{
		auto layer = *tilesBegin;
		int layerWidth = layer.size();
		int layerHeight = layer[0].size();

		for (int x = (cameraLeft < 0 ? 0 : cameraLeft); x < (cameraRight < layerWidth ? cameraRight : layerWidth); x++)
		{
			for (int y = (cameraTop < 0 ? 0 : cameraTop); y < (cameraBottom < layerHeight ? cameraBottom : layerHeight); y++)
			{
				if (layer[x][y] != nullptr)
				{
					_renderEngine->Draw(layer[x][y]);
				}
			}
		}
	}

	auto components = _level->GetComponents();
	for (Component* comp : *components)
	{
		if (comp != nullptr)
		{
			_renderEngine->Draw(comp);
		}
	}
	tiles = nullptr;
	components = nullptr;


	_uiManager->Draw(interpolate);
}

void LevelManager::LoadLevel(std::string levelname)
{
	_level = new Level();
	Tmx::Map* map = new Tmx::Map();
	map->ParseFile(levelname);

	LoadTiles(map);
	LoadEntities(map);


	std::cout << "Level loaded. " << std::endl;
}

void LevelManager::SetPhysics(Component* component, b2Vec2 Pos, b2Vec2 Size, b2BodyType bodyType)
{
	b2BodyDef bodyDef;
	bodyDef.type = bodyType;
	bodyDef.position.Set((Pos.x + (Size.x / 2)) / SCALING_FACTOR, Pos.y / SCALING_FACTOR);
	bodyDef.angle = 0;

	b2Body* dynamicBody = _level->GetWorld()->CreateBody(&bodyDef);

	b2PolygonShape boxShape;
	boxShape.SetAsBox((Size.x / 2) / SCALING_FACTOR, (Size.y / 2) / SCALING_FACTOR);

	b2FixtureDef boxFixtureDef;
	boxFixtureDef.shape = &boxShape;
	boxFixtureDef.density = 1;
	boxFixtureDef.friction = 0.5;
	boxFixtureDef.restitution = 0;

	dynamicBody->CreateFixture(&boxFixtureDef);
	dynamicBody->SetAwake(true);

	//add foot sensor fixture
	/*boxShape.SetAsBox(0.3, 0.3, b2Vec2(0, -2), 0);
	boxFixtureDef.isSensor = true;
	b2Fixture* footSensorFixture = dynamicBody->CreateFixture(&boxFixtureDef);
	footSensorFixture->SetUserData((void*)2);
*/
	component->SetBody(dynamicBody);
}

void LevelManager::LoadTiles(Tmx::Map* map)
{
	const Tmx::Tileset* tileset;
	tileset = map->GetTileset(0);
	_level->SetTileset(tileset);

	SDL_Texture* surf = _resourceManager->LoadTexture(map->GetFilepath() + tileset->GetImage()->GetSource());
	if (surf == nullptr)
	{
		std::cout << "Map loaded" << std::endl << "Tileset not loaded" << std::endl;
		return;
	}

	std::vector<std::vector< std::vector<Component*>>> tiles;
	tiles.resize(map->GetLayers().size());

	for (unsigned int i = 0; i < map->GetLayers().size(); i++)
	{
		const Tmx::Layer* layer = map->GetLayer(i);
		int Width = layer->GetWidth();
		int Height = layer->GetHeight();

		std::string layername = layer->GetName();
		transform(layername.begin(), layername.end(), layername.begin(), tolower);

		if (layer->IsVisible() == 0)
		{
			continue;
		}

		tiles[i].resize(Width, std::vector<Component*>(Height, nullptr));
		for (int x = 0; x < Width; x++)
		{
			for (int y = 0; y < Height; y++)
			{
				Tmx::MapTile tile = layer->GetTile(x, y);


				/*int tileset_col = ((tile.id - 1) % Num_Of_Cols);
				int tileset_row = ((tile.id - 1) / Num_Of_Cols);*/

				int CurrentTile = tile.id;

				if (CurrentTile == 0)
				{
					continue;
				}
				////Check if the current tile exist within the tileset, if not restart loop.
				//CurrentTile -= map->GetTileset(0)->GetFirstGid() - 1;

				//Calculate specific Position of the currentile in the Map         
				int tileset_col = (CurrentTile % (map->GetTileset(0)->GetImage()->GetWidth() / map->GetTileset(0)->GetTileWidth()));
				int tileset_row = (CurrentTile / (map->GetTileset(0)->GetImage()->GetWidth() / map->GetTileset(0)->GetTileWidth()));

				b2Vec2 Pos;
				Pos.x = x * tileset->GetTileWidth();
				Pos.y = y * tileset->GetTileHeight();

				b2Vec2 Size;
				Size.x = tileset->GetTileWidth();
				Size.y = tileset->GetTileHeight();

				SDL_Rect Clip;
				Clip.y = (map->GetTileset(0)->GetMargin() + (map->GetTileset(0)->GetTileHeight() + map->GetTileset(0)->GetSpacing()) * tileset_row);
				Clip.x = (map->GetTileset(0)->GetMargin() + (map->GetTileset(0)->GetTileWidth() + map->GetTileset(0)->GetSpacing()) * tileset_col);
				Clip.w = map->GetTileset(0)->GetTileWidth();
				Clip.h = map->GetTileset(0)->GetTileHeight();


				Component* tileComponent = new Component();
				tileComponent->SetPosition(Pos);
				tileComponent->SetSize(Size);
				tileComponent->SetClipping(&Clip);
				tileComponent->SetTexture(surf);

				if (layername == "collision")
				{
					SetPhysics(tileComponent, Pos, Size, b2_staticBody);
				}

				tiles[i][x][y] = tileComponent;
			}
		}
	}

	_level->SetTiles(tiles);
}

void LevelManager::LoadEntities(Tmx::Map* map)
{
	std::vector<Component*> components;


	components.resize(map->GetObjectGroups().size());

	for (unsigned int i = 0; i < map->GetObjectGroups().size(); i++)
	{
		int objectsInLayer = map->GetObjectGroup(i)->GetObjects().size();

		std::string layername = map->GetObjectGroup(i)->GetName();

		components.resize(components.size() + objectsInLayer);

		for (unsigned int x = 0; x < objectsInLayer; x++)
		{
			const Tmx::Object* obj = map->GetObjectGroup(i)->GetObject(x);

			b2Vec2 pos;
			pos.x = obj->GetX();
			pos.y = obj->GetY();

			b2Vec2 size;
			size.x = obj->GetWidth();
			size.y = obj->GetHeight();

			if (obj->GetType() == "Player" ||
				obj->GetType() == "Actor" ||
				obj->GetType() == "Enemy" ||
				layername == "collision")
			{
				SDL_Rect clip;
				clip.x = 0;
				clip.y = 0;
				clip.w = obj->GetWidth();
				clip.h = obj->GetHeight();

				Entity* entity = new Entity();

				if (layername != "collision")
				{
					std::string filename = "assets/images/" + map->GetObjectGroup(i)->GetObject(x)->GetProperties().GetLiteralProperty("filename");
					SDL_Texture* tex = _resourceManager->LoadTexture(filename);
					entity->SetTexture(tex);
				}
				else
				{
					std::cout << "Collision entity spawning: { w:" << size.x << ", h:" << size.y << " }" << std::endl;
				}
				entity->SetPosition(pos);
				entity->SetSize(size);
				entity->SetClipping(&clip);

				SetPhysics(entity, pos, size, layername == "collision" ? b2_staticBody : b2_dynamicBody);

				components.push_back(entity);

				if (obj->GetType() == "Player")
				{
					_player = entity;
				}
			}

			/*if (map->GetObjectGroup(i)->GetObject(x)->GetType() == "Trigger")
			{
			std::string name = map->GetObjectGroup(i)->GetObject(x)->GetName();
			std::string attr1 = map->GetObjectGroup(i)->GetObject(x)->GetProperties().GetLiteralProperty("attr1");

			TriggerType type = (TriggerType)atoi(map->GetObjectGroup(i)->GetObject(x)->GetProperties().GetLiteralProperty("type").c_str());

			TriggerZone* zoneToAdd;

			switch (type)
			{
			case Save:
			zoneToAdd = new SaveZone(pos, name, attr1);
			break;
			case Kill:
			zoneToAdd = new KillZone(pos, name, attr1);
			break;
			case TriggerEnd:
			zoneToAdd = new EndZone(pos, name, attr1);
			break;
			default:
			zoneToAdd = new TriggerZone(pos, name, attr1);
			}

			triggers.push_back(zoneToAdd);
			}*/
		}
	}

	_level->SetComponents(components);
}

Entity* LevelManager::GetPlayer()
{
	return _player;
}

void LevelManager::UpdateCamera()
{
	SDL_Rect* camera = _renderEngine->GetCamera();

	b2Vec2 playerPos = _player->GetPosition();

	//TODO: GET BETTER VARIABLES!
	camera->x = playerPos.x - 640 / 2.0;
	camera->y = playerPos.y - 480 / 2.0;
}
