#pragma once
#include "library.h"

class Component
{
protected:
	b2Vec2 _pos;
	b2Vec2 _size;
	SDL_Texture* _texture;
	SDL_Rect _clipping;
	bool _isNearScreen;

	//Physics stuffs
	b2Body* _body;
	b2Vec2 _velocity;
	bool _velocityChanged;

	int _health;
public:
	Component();
	~Component();

	b2Vec2 GetPosition();
	b2Vec2 GetSize();
	virtual void Update(int interpolate);
//	void Draw(int interpolate);
	void SetPosition(b2Vec2 position);
	void SetSize(b2Vec2 size);
	void SetTexture(SDL_Texture* texture);
	SDL_Texture* GetTexture();
	SDL_Rect* GetClipping();
	void SetClipping(SDL_Rect* clipping);

	void SetBody(b2Body* body);
	SDL_Rect* GetRect();
	void ApplyVelocity(b2Vec2 vel);
	bool GetIsNearScreen();
	int GetHealth();
//	void SetHealth(int health);
	void InflictDamage(int damage);
};
