#include "library.h"
#include "LevelManager.h";
#include "InputManager.h"

const char* SCREEN_TITLE = "";

bool GAME_IS_RUNNING = true;

float START_CLOCK;
float GAME_TIME;
float LAST_TIME;

float SKIP_TICKS = 1000 / UPDATES_PER_SECOND;
float MAX_FRAMESKIP = 5;

Uint32 FLAGS = SDL_WINDOW_OPENGL;

void logSDLError(std::ostream &os, const std::string &msg){
	os << msg << " error: " << SDL_GetError() << std::endl;
}

bool init()
{
	if (FULLSCREEN)
	{
		FLAGS |= SDL_WINDOW_FULLSCREEN;
	}

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0){
		logSDLError(std::cout, "SDL_Init");
		return false;
	}

	if (TTF_Init() == -1) {
		logSDLError(std::cout, "TTF_Init error!");
		return false;
	}

	return true;
}

int main(int argc, char **argv)
{
	if (!init())
	{
		return 1;
	}

	SDL_Window *window = SDL_CreateWindow(SCREEN_TITLE, 
		100, 
		100, 
		VIEWPORT_WIDTH,
		VIEWPORT_HEIGHT, 
		FLAGS);

	if (window == nullptr)
	{
		logSDLError(std::cout, "CreateWindow");
		return 2;
	}


	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	if (renderer == nullptr)
	{
		logSDLError(std::cout, "CreateRenderer");
		return 3;
	}

	START_CLOCK = SDL_GetTicks();

	double NEXT_GAME_TICK = SDL_GetTicks();
	int LOOPS = 0;
	float INTERPOL = 0;
	int drawfpstick = SDL_GetTicks();

	LevelManager* lvlMgr = new LevelManager(renderer);
	InputManager* inpMgr = new InputManager(lvlMgr);

	while (GAME_IS_RUNNING)
	{
		LOOPS = 0;
		while (SDL_GetTicks() > NEXT_GAME_TICK && LOOPS < MAX_FRAMESKIP)
		{
			GAME_TIME = SDL_GetTicks();
			GAME_IS_RUNNING = inpMgr->PollEvent(GAME_IS_RUNNING);
			lvlMgr->Update(INTERPOL);

			NEXT_GAME_TICK += SKIP_TICKS;
			LOOPS++;

			if (GAME_TIME > LAST_TIME + 1000)
			{
				LAST_TIME = GAME_TIME;
			}
		}
		SDL_RenderClear(renderer);
		lvlMgr->Draw(INTERPOL);
		SDL_RenderPresent(renderer);
	}


	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}